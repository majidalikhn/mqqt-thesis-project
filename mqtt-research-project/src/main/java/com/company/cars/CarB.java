package com.company.cars;

import com.company.util.SpeedGenerator;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

public class CarB implements MqttCallback {

    public static void main(String[] args) {

        try {
            new CarB().runClient();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void runClient() throws Exception {
        String broker = "tcp://mqtt.eclipse.org:1883";
        MqttClient client = new MqttClient(broker, "CarB");
        client.connect();
        client.setCallback(this);
        client.subscribe("speed");
        MqttMessage message = new MqttMessage();
        JSONObject json = new JSONObject();
        json.put("clientID", "CarB");

        while (true) {
            Thread.sleep(1000);
            int speed = SpeedGenerator.generate(0, 120);
            json.put("speed", speed);
            message.setPayload(json.toString().getBytes());
            client.publish("speed", message);
        }
    }

    public void connectionLost(Throwable throwable) {
        System.out.println("Connection to MQTT broker lost!");
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        JSONObject json = new JSONObject(new String(mqttMessage.getPayload()));
        int speedA = 0;
        if (json.get("clientID").equals("CarA")) {
            speedA = Integer.valueOf((Integer) json.get("speed"));
        }

        System.out.println(json.toString());


    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
    }
}
